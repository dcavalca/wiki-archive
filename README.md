# CentOS Wiki archive

This repository holds an archival copy of the [CentOS Wiki](https://wiki.centos.org/) for historical preservation. The archive was generated with [Browsertrix Crawler](https://github.com/webrecorder/browsertrix-crawler) in [WACZ](https://replayweb.page/docs/wacz-format) format. This archive is then loaded with [ReplayWeb.page](https://replayweb.page/docs/) for replay and embedded into a static website deployed to [GitLab Pages](https://dcavalca.gitlab.io/wiki-archive).

## License

The fronted website is under MIT per the included LICENSE file. ReplayWeb.page is AGPLv3, and bundles a copy of Ruffle, which is under MIT. The contents of the wiki included in the archive are under the Creative Commons Attribution-Share Alike 3.0 Unported License.
